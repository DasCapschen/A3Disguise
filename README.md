# A3 Disguise
I made this script to recreate the functionality seen in early previews ( https://www.youtube.com/watch?v=efC6gAO6exA  at 1:50 ).  
The systems in this script try to recreate the feel of "Death to Spies". Meaning you can steal different uniforms to get into different areas (work in progress) and enemies get suspicious when you show up wearing the wrong gear or holding an opfor weapon.  

## Multiplayer 
This is script may or may not work in multiplayer. It is really old and I am in progress of completely reworking it, see GlobalMobilization branch.  

## Branches
This branch is for the vanilla game (no mods, no dlc) and uses a really old and probably half-broken version of the script.  
Check "global mobilization" for a more up-to-date version of the script and GM support.  

## Mods
This script cannot possibly be compatible with mods because checking gear requires manually making a list of the classnames (since weapons and gear are not ususally sorted by sides).  
Feel free to modify the script to include classnames of any mods you use.
